package basics.variables

fun main(args: Array<String>) {

    // declaring vals cannot be modified
    val question = "The Ultimate Question of Life, the Universe, and Everything"

    // this will fail
    // question = "Any other"

    // to declare a int type
    val age: Int = 42

    val yearsToCompute = 7.5e6 // 7.5 * 10 ^ 6 = 7600000.0

    val answerForAge: Int
    answerForAge = 42

    // val (from value) -- can't be modified
    // var (from variable) -- it can be modified

    // by default declara all variables with val


    // this block shows how to use vals and vars
    val canPerformeOperation: Boolean = true

    var message = "Success"

    if (canPerformeOperation) {
        message = "Success"
    } else{
        message = "Failed"
    }


    // a is imutable, so in this case kotlin uses references
    val languages = arrayListOf("Java")
    languages.add("Kotlin")
    print(languages)

    // types are important
    var myAge = 32

    // it will fail with a TypeMissMatch error
    // myAge = "no_age"



}
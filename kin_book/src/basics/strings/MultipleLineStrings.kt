package basics.strings

fun main(args: Array<String>) {
    val kotlinLogo = """|  //
                       .| //
                       .| / \ """

    println(kotlinLogo.trimMargin("."))
}
package basics.collections

fun main(args: Array<String>) {

    val list = arrayListOf(1, 7, 53)
    val map = hashMapOf(1 to "one", 7 to "seven", 53 to "fifity-three")
    val set = hashSetOf(1, 7, 53)

    println(set.javaClass)
    println(map.javaClass)
    println(list.javaClass)

    val strings = listOf("first", "second", "fourteenth")
    print(strings.last())

    val numbers = setOf(1, 14, 2)
    println(numbers.max())
}
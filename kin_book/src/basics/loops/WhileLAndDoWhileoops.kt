package basics.loops

fun main(args: Array<String>) {

    var n: Int = 0

    while (n <= 10) {
        println("n value now is $n")
        n++
    }

    n = 0
    println()

    do {
        println("n value now is $n")
        n++
    } while (n <= 10)

}
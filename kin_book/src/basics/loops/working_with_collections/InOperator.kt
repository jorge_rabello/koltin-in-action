package basics.loops.working_with_collections

fun main(args: Array<String>) {
    println(isLetter('A'))
    println(isLetter('8'))

    println(isNotDigit('A'))
    println(isNotDigit('8'))

    println(recognize('A'))
    println(recognize('1'))
    println(recognize('#'))
    println(recognize(' '))

    val langs = setOf("Java", "Kotlin", "Scala")
    println("Kotlin" in langs)
    println("Kotlin" in "Java".."Scala")

    println("Kotlin" in setOf("Java", "Scala"))

}

fun isLetter(c: Char) = c in 'a'..'z' || c in 'A'..'Z'
fun isNotDigit(c: Char) = c !in '0'..'9'

fun recognize(c: Char) = when (c) {
    in '0'..'9' -> "It's a digit !"
    in 'a'..'z', in 'A'..'Z' -> "It's a letter !"
    else -> "It's a symbol"
}

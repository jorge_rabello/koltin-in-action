package basics.loops.working_with_maps

import java.util.*


/**
 * in this exercise we'll print the binary equivalent values to each letters in range of A to F
 */
fun main(args: Array<String>) {

    val binaryReps = TreeMap<Char, String>()

    for(c in 'A'..'F') {
        val binary = Integer.toBinaryString(c.toInt())

        binaryReps[c] = binary // it's equivalent to binaryReps.put(c, binary) in Java
    }

    for ((letter, binary) in binaryReps) {
        println("$letter = $binary")
    }
}



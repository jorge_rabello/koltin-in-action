package basics.loops

fun main(args: Array<String>) {

    val oneToTen = 1..10

    for (i in oneToTen) {
        println("The value now is $i")
    }
}
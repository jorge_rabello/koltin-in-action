package basics.functions

fun main(args: Array<String>) {
    val list = listOf(1, 2, 3)
    println(joinToString(list, separator = "; ", prefix = "(", suffix = ")"))
    println(joinToString(list, ", ", "", ""))
    println(joinToString(list, "; "))
    println(joinToString(list, suffix = ";", prefix = "# "))
}

fun <T> joinToString(
    collection: Collection<T>,
    separator: String = ", ",
    prefix: String = "",
    suffix: String = ""
): String {

    val result = StringBuilder(prefix)

    for ((index, element) in collection.withIndex()) {

        if (index > 0) {
            result.append(separator)
        }

        result.append(element)

    }

    result.append(suffix)
    return result.toString()

}
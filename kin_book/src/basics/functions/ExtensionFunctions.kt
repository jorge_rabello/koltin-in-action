package basics.functions

import basics.functions.strings.join
import basics.functions.strings.lastChar as last

fun main(args: Array<String>) {

    println("Kotlin".last())

    val list = listOf(1, 2, 3)

    println(list.joinToString(separator = "; ", prefix = "(", postfix = ")"))
    println(list.joinToString(" "))

    val strings = listOf("one", "two", "eight").join(" ")

    println(strings)


}


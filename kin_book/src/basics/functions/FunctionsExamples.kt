package basics.functions

fun main(args: Array<String>) {

    println(max(10, 5))
    println(maxExpression(10, 5))

}

// functions with return and parameters
    fun max(a: Int, b: Int) : Int {
        return if (a > b) a else b
    }

// expression bodies
// a mesma função acima pode ser escrita assim:
fun maxExpression(a: Int, b: Int) = if (a > b) a else b

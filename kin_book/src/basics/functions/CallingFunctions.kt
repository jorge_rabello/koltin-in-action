package basics.functions

import basics.functions.strings.joinToString

fun main(args: Array<String>) {

    val list = listOf(1, 2, 3)
    println(list.joinToString("; ", "(", ")"))
    println(list.joinToString(", ", "", ""))
    println(list.joinToString(", "))
    println(list.joinToString())
    println(list.joinToString("; "))

    println(list.joinToString(separator = "; ", prefix = "# "))

}

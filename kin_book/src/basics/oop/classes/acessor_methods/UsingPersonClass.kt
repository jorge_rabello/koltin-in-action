package basics.oop.classes.acessor_methods

fun main(args: Array<String>) {

    val jorge = Person("Jorge", true)

    println("Name: " + jorge.name)
    println("Is Married: " + jorge.isMarried)

    // see that this will fail (we can't change our names (read-only - has a getter without a setter)
    // jorge.name = "Caio"

    // but this is ok - we can change relationship status (writable - has getter and setter)
    jorge.isMarried = false


    println("Name: " + jorge.name)
    println("Is Married: " + jorge.isMarried)
}
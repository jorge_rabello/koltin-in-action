package basics.oop.classes.acessor_methods

/**
 * in this case name is a read-only property: generates a field and a trivial getter
 * and isMarried is a writable property: generates a field, a getter and a setter
 */
class Person(
    val name: String,
    var isMarried: Boolean
)
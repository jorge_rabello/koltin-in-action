package basics.oop.classes.custom_acessors

fun main(args: Array<String>) {

    val shape = Shape(10, 10)
    println(shape.isSquare)

    val otheShape = Shape(10, 20)
    println(otheShape.isSquare)
}
package basics.oop.classes.custom_acessors

class Shape(val height: Int, val width: Int) {

    // this getter checks if the shape is a square
    // we can improve writing in this format - without the curly braces
    val isSquare: Boolean get() = height == width
}
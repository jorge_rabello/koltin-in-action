package basics.oop.classes.custom_acessors

import kotlin.random.Random

fun createRandomRectangle(): Shape {
    val randomWidth = Random.nextInt(0, 100)
    val randomHeight = Random.nextInt(0, 100)
    return Shape(randomHeight, randomWidth)
}
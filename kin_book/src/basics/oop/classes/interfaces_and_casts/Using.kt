package basics.oop.classes.interfaces_and_casts

fun main(args: Array<String>) {
    println(eval(Sum(Sum(Num(1), Num(2)), Num(4)))) // should return 7 (4+2+1)
    println(evalWithLogging(Sum(Sum(Num(1), Num(2)), Num(4)))) // should return 7 (4+2+1)
}

// this function evaluates a expression
fun eval(e: Expr): Int = when (e) {
    is Num -> e.value
    is Sum -> eval(e.right) + eval(e.left)
    else -> throw  IllegalArgumentException("Unknown expression")
}

// in this case we need to use curly braces, cause we have two instructions
// here we have a rule: the last expression in a block is the result
fun evalWithLogging(e: Expr): Int = when (e) {
    is Num -> {
        println("num: ${e.value}")
        e.value
    }
    is Sum -> {
        val left = evalWithLogging(e.left)
        val right = evalWithLogging(e.right)
        println("sum: $left + $right")
        left + right
    }
    else -> throw IllegalArgumentException("Unknown expression")
}

// the is operador is like instance of in Java - so you can check whether a variable is of a certain type
// the as operador is used to make a explicit cast like this in Java ->  c = (int) b
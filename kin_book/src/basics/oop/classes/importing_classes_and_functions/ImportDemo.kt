package basics.oop.classes.importing_classes_and_functions

import basics.oop.classes.custom_acessors.Shape
import basics.oop.classes.custom_acessors.createRandomRectangle

fun main(args: Array<String>) {
    // note that Shape is a class that is in other package so it needs to be imported (line 3)
    val shape = Shape(10, 20)

    // note that we can import functions directly also, thats very nice !!!
    val randomShape = createRandomRectangle()
    println(randomShape.isSquare)

}
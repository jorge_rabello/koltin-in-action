package basics.oop.classes.enums.using_enums_and_imports

import basics.oop.classes.enums.Color
import basics.oop.classes.enums.Color.*


/**
 * we can use imports as you can see in lines 3 and 4
 * so we don't need to call Color.VIOLET and use only VIOLET - so far so good
 */
fun main(args: Array<String>) {

    println(getMnemonic(VIOLET))
    println(getWarmth(RED))
}


fun getMnemonic(color: Color) = when (color) {
    RED -> "Romeo"
    ORANGE -> "Oscar"
    YELLOW -> "Yankee"
    GREEN -> "Golf"
    BLUE -> "Bravo"
    INDIGO -> "India"
    VIOLET -> "Victor"
}

fun getWarmth(color: Color) = when (color) {
    RED, Color.ORANGE, Color.YELLOW -> "warm"
    GREEN -> "neutral"
    BLUE, Color.INDIGO, Color.VIOLET -> "cold"
}
package basics.oop.classes.enums.using_when_without_argument

import basics.oop.classes.enums.Color
import basics.oop.classes.enums.Color.*

fun main(args: Array<String>) {

    println(mixOptimized(BLUE, YELLOW))
    println(mixOptimized(BLUE, GREEN))

}

fun mixOptimized(c1: Color, c2: Color) = when {
    (c1 == RED && c2 == YELLOW)  || (c1 == YELLOW && c2 == RED) -> ORANGE

    (c1 == YELLOW && c2 == BLUE)  || (c1 == BLUE && c2 == YELLOW) -> GREEN

    (c1 == BLUE && c2 == VIOLET)  || (c1 == VIOLET && c2 == BLUE) -> INDIGO

    else -> throw Exception("Dirty color")
}
package basics.oop.classes.enums

fun main(args: Array<String>) {

    println(Color.BLUE)

    println(Color.RED.r)

    println(Color.RED.g)

    println(Color.RED.b)

    println(Color.INDIGO.rgb())
    println(Color.BLUE.rgb())

}
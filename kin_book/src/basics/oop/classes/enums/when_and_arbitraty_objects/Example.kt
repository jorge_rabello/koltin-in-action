package basics.oop.classes.enums.when_and_arbitraty_objects

import basics.oop.classes.enums.Color
import basics.oop.classes.enums.Color.*

fun main(args: Array<String>) {

    println(mix(YELLOW, BLUE))

    println(mix(RED, BLUE))
}

fun mix(c1: Color, c2: Color) = when (setOf(c1, c2)) {
    setOf(RED, YELLOW) -> ORANGE
    setOf(YELLOW, BLUE) -> GREEN
    setOf(BLUE, VIOLET) -> INDIGO
    else -> throw Exception("Dirty color")
}
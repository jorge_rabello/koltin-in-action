package basics.oop.classes.enums

fun main(args: Array<String>) {

    println(getMnemonic(Color.VIOLET))
    println(getWarmth(Color.RED))
}


fun getMnemonic(color: Color) = when (color) {
    Color.RED -> "Romeo"
    Color.ORANGE -> "Oscar"
    Color.YELLOW -> "Yankee"
    Color.GREEN -> "Golf"
    Color.BLUE -> "Bravo"
    Color.INDIGO -> "India"
    Color.VIOLET -> "Victor"
}

fun getWarmth(color: Color) = when (color) {
    Color.RED, Color.ORANGE, Color.YELLOW -> "warm"
    Color.GREEN -> "neutral"
    Color.BLUE, Color.INDIGO, Color.VIOLET -> "cold"
}
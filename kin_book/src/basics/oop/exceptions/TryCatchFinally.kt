package basics.oop.exceptions

import java.io.BufferedReader
import java.io.StringReader

/**
 * in this example different of Java we don't need to write a throws declaration
 */
fun main(args: Array<String>) {
    val reader = BufferedReader(StringReader("abc"))
    println(readANumber(reader))
}

fun readANumber(reader: BufferedReader) {

    // try catch expression
    val number = try {
        // if no exceptions use this for number
        Integer.parseInt(reader.readLine())
    } catch (e: NumberFormatException) {
        // if an exception is thrown prints null
        null
    }
    println(number)
}
package collections_functions

fun alphabet2(): String {
    val result = StringBuilder()
    for (letter in 'A'..'Z') {
        result.append(letter)
    }
    result.append("\nNow I know the alphabet!")
    return result.toString()
}


// we can do same thing this way
fun improvedAlphabet2() = StringBuilder().apply {
    for (letter in 'A'..'Z') {
        append(letter)
    }
    append("\nNow I know the alphabet!")
}.toString()


fun main() {

    println(alphabet2())
    println(improvedAlphabet2())
}
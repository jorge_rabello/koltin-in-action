package collections_functions

data class Person(val name: String, val age: Int)

package collections_functions

fun main() {
    val people = listOf(Person("Alice", 31), Person("Bob", 29), Person("Carol", 31))

    val list = people.asSequence()
        .map(Person::name)
        .filter { it.startsWith("A") }
        .toList()

    println(list)

    listOf(1, 2, 3, 4).asSequence()
        .map { print("map($it) "); it * it }
        .filter { print("filter($it) "); it % 2 == 0 }
        .toList()

    val vipList = listOf(
        Person("Alice", 29),
        Person("Bob", 31),
        Person("Charles", 31),
        Person("Dan", 21)
    )

    println()
    println(vipList.asSequence().map(Person::name).filter { it.length < 4 }.toList())
    println(vipList.asSequence().filter { it.name.length < 4 }.map(Person::name).toList())
}
package collections_functions

val naturalNumbers = generateSequence(0) { it + 1 }
val naturalNumbersTo100 = naturalNumbers.takeWhile { it <= 100 }

fun main() {
    println(naturalNumbersTo100.toList())

    println(naturalNumbersTo100.sum())
}
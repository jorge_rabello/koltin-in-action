package collections_functions

fun main() {

    val people = listOf(Person("Alice", 27), Person("Bob", 31))
    println(people.all(canBeInClub27))
    println(people.any(canBeInClub27))
    println(people.count(canBeInClub27))
    println(people.find(canBeInClub27))
}

val canBeInClub27 = { p: Person -> p.age <= 27 }
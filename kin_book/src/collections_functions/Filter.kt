package collections_functions

fun main() {

    val list = listOf(1, 2, 3, 4)

    // filter function
    println(list.filter { it % 2 == 0 })

    val people = listOf(Person("Alice", 29), Person("Bob", 31))
    println(people.filter { it.age > 30 })
}
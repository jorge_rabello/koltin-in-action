package collections_functions

fun alphabet(): String {
    val result = StringBuilder()
    for (letter in 'A'..'Z') {
        result.append(letter)
    }
    result.append("\nNow I know the alphabet!")
    return result.toString()
}


// we can do same thing this way
fun improvedAlphabet() = with(StringBuilder()) {
    for (letter in 'A'..'Z') {
        this.append(letter)
    }
    append("\nNow I know the alphabet!")
        .toString()
}


fun main() {

    println(alphabet())
    println(improvedAlphabet())
}
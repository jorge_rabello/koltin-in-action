package collections_functions

fun main() {
    val list = listOf(1, 2, 3, 4)
    println(list.map { it * it })

    val people = listOf(Person("Alice", 29), Person("Bob", 31))
    println(people.map { it.name })
    println(people.map(Person::name))


    // chain calls
    people.filter { it.age > 30 }.map(Person::name)

    val maxAge = people.maxBy(Person::age)?.age

    println(people.filter { it.age == maxAge })

    val numbers = mapOf(0 to "zero", 1 to "one")
    println(numbers.mapValues { it.value.toUpperCase() })
}


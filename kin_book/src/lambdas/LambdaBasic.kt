package lambdas

fun main() {
    println(sum(1, 2))
    println(sumWithMessage(1, 2))

    val erros = listOf("403 Forbidden", "404 Not Found")
    printWithMessageWithPrefix(erros, "Error: ")

    val responses = listOf("200 OK", "418 I'm a teapot", "500 Internal Server Error")

    printProblemCounts(responses)

}

val sum = { x: Int, y: Int -> x + y }

val sumWithMessage = { x: Int, y: Int ->
    println("Computing the sum of $x and $y...")
    x + y
}

fun printWithMessageWithPrefix(messages: Collection<String>, prefix: String) {
    messages.forEach {
        println("$prefix $it")
    }
}

fun printProblemCounts(responses: Collection<String>) {
    var clientErrors = 0
    var serverErrors = 0
    responses.forEach {
        if (it.startsWith("4")) {
            clientErrors++
        } else if (it.startsWith("5")) {
            serverErrors++
        }
    }
    println("$clientErrors client errors, $serverErrors, server errors")
}

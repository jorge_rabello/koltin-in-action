package lambdas

fun main() {

    //lambda expression
    val getAgeWithLambda = { person: Person -> person.age }

    // make the same thing using member reference
    val getAge = Person::age

    // member reference with functions
    run(::salute)

    // with lambda
    val nextActionLambda = action

    // with member reference
    val nextAction = ::sendEmail

    val createPerson = ::Person
    val p = createPerson("Jorge", 29)
    println(p)

    val predicate = Person::isAdult

}


fun salute() = println("Salute !")

val action = { person: Person, message: String ->
    sendEmail(person, message)
}

fun sendEmail(person: Person, message: String) {
    print("Sending message to $person")
}

fun Person.isAdult() = age >= 21

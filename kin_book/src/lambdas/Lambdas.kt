package lambdas

fun main() {

    val people = listOf(Person("Alice", 29), Person("Bob", 31))

    // using a complicated function
    findTheOldest(people)

    // doing the same thing
    println(people.maxBy { it.age })
    println(people.maxBy(Person::age))
    println(people.maxBy { p: Person -> p.age })
    println(people.maxBy { p -> p.age })

    val names = people.joinToString(separator = " ", transform = { p: Person -> p.name })
    val sameNames = people.joinToString(" ") { p: Person -> p.name }
    println(names)
    println(sameNames)

    val getAge = { p: Person -> p.age }
    println(people.maxBy(getAge))
}

fun findTheOldest(people: List<Person>) {

    var maxAge = 0
    var theOldest: Person? = null
    for (person in people) {
        if (person.age > maxAge) {
            maxAge = person.age
            theOldest = person
        }
    }
    println(theOldest)
}
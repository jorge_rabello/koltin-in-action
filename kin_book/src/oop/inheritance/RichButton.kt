package oop.inheritance

import oop.interfaces.Clickable

/**
 * only just open classes and methods can be inherited
 */
open class RichButton : Clickable {

    fun disable() {}

    open fun animate() {}

    override fun click() {

    }
}
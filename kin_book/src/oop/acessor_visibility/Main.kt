package oop.acessor_visibility

fun main(args: Array<String>) {
    val lengthCounter = LengthCounter()
    lengthCounter.addWord("Hi !")
    print(lengthCounter.counter)

    // It'll be failed with message : Cannot assign to 'counter': the setter is private in 'LengthCounter'
    // because de setter for this property is private
    // lengthCounter.counter = 3
}
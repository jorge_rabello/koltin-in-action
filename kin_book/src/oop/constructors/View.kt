package oop.constructors

import javax.naming.Context
import javax.print.attribute.AttributeSet

open class View {

    // secondary constructors
    constructor(ctx: Context) {
        // some code
    }


    constructor(ctx: Context, attr: AttributeSet) {
        // some code
    }
}
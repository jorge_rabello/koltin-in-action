package oop.constructors

import javax.naming.Context
import javax.print.attribute.AttributeSet

class MyButton : View {

    constructor(ctx: Context) : super(ctx) {
        // some code
    }

    constructor(ctx: Context, attr: AttributeSet) : super(ctx, attr) {
        // some code
    }

}
package oop.constructors

open class User(
    val nickname: String,
    val isSubscribed: Boolean = false)
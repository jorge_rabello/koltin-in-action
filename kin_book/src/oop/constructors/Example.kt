package oop.constructors

fun main(args: Array<String>) {
    val alice = User("Alice")
    print(alice.isSubscribed)

    val bob = User("Bob", true)
    println(bob.isSubscribed)

    val carol = User("Carol", isSubscribed = true)
    println(carol.isSubscribed)

}
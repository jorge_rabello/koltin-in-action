package oop.tostring

class Client(private val name: String, private val postalCode: Int) {

    override fun toString(): String = "Cliente(name=$name, postalCode=$postalCode)"
    fun copy(name: String = this.name, postalCode: Int = this.postalCode) = Client(name, postalCode)
}

fun main() {

    val bob = Client("Bob", 973293)
    println(bob.copy(postalCode = 382555))
}
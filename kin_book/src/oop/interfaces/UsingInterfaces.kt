package oop.interfaces

fun main(args: Array<String>) {

    val button = Button()
    button.showOff()
    button.setFocus(true)
    button.click()

}
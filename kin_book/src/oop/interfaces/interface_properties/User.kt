package oop.interfaces.interface_properties

interface User {

    val email: String

    // in this case every time that getNickname() was called the result value will be computed
    val nickname: String get() = email.substringBefore('@')
}
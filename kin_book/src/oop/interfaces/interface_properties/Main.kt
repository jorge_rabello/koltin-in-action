package oop.interfaces.interface_properties

fun main(args: Array<String>) {
    println(PrivateUser("test", "test@kotlinlang.org").nickname)
    println(SubscribingUser("test@kotlinlang.org").nickname)
}
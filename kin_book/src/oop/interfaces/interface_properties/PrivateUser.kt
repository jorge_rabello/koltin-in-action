package oop.interfaces.interface_properties

class PrivateUser(override val nickname: String, override val email: String) : User

class SubscribingUser(override val email: String) : User {

    override val nickname: String
        get() = email.substringBefore('@')
}

class FacebookUser(accountId: Int) : User {

    override val email: String
        get() = "bazinga@mail.com"

    override val nickname = getFacebookName(accountId)

    fun getFacebookName(accountId: Int): String {
        return "bazinga"
    }
}


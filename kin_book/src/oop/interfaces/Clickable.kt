package oop.interfaces

interface Clickable {

    fun click()

    // here a method with a default implementation
    fun showOff() = println("I'm clickable !")
}

package oop.backing_fields

fun main(args: Array<String>) {
    val user = User("Alice")
    user.address = "Elsenheimerstrasse, 47, 80687 Muenchen"
}
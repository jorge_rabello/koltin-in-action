package oop.non_trivial_constructors

import jdk.nashorn.internal.runtime.Context
import javax.swing.text.AttributeSet

class MyButton : View {

    constructor(ctx: Context) : super(ctx) {
        // some code
    }

    // this is optional
    constructor(ctx: Context, attr: AttributeSet) : super(ctx, attr) {
        // some code
    }
}
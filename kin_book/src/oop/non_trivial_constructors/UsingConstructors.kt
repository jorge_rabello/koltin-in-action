package oop.non_trivial_constructors

fun main(args: Array<String>) {

    val alice = User("Alice")
    val bob = User("Bob", false)
    val carol = User("Carol", isSubscribed = false)


    println(alice.isSubscribed)
    println(bob.isSubscribed)
    println(carol.isSubscribed)

}
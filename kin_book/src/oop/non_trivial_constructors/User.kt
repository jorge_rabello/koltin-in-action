package oop.non_trivial_constructors

open class User constructor(
    val nickname: String,
    val isSubscribed: Boolean = true
)
package oop.non_trivial_constructors

import jdk.nashorn.internal.runtime.Context
import javax.swing.text.AttributeSet

open class View {

    constructor(ctx: Context) {
        // some code
    }

    constructor(ctx: Context, attr: AttributeSet){
        // some code
    }
}